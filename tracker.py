#!/usr/bin/env python3

import cProfile
import glob
import time
import sys
import logging
import socket
import struct
import math

import numpy as np
import cv2

RESOLUTION = (640, 480)

# https://stackoverflow.com/questions/54970421/python-opencv-solvepnp-convert-to-euler-angles
def rot_params_rv(rvecs):
    from math import pi,atan2,asin
    R = cv2.Rodrigues(rvecs)[0]
    roll = 180*atan2(-R[2][1], R[2][2])/pi
    pitch = 180*asin(R[2][0])/pi
    yaw = 180*atan2(-R[1][0], R[0][0])/pi
    rot_params= [roll,pitch,yaw]
    return rot_params


def capture_calibration():
    cam = cv2.VideoCapture(0)
    cam.set(cv2.CAP_PROP_FPS, 60);
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, RESOLUTION[0]);
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, RESOLUTION[1]);
    # cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640);
    # cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480);

    for i in range(0, 20):
        for n in range(0, 10):
            ret_val, img = cam.read()
        time.sleep(0.1)
        ret_val,img = cam.read()
        cv2.imshow("webcam", img)
        cv2.imwrite("calib_{}.jpg".format(i), img)
        if cv2.waitKey(1) == 27:
            break
        input()
        print("next loop")
    cv2.destroyAllWindows()



def calibrate():
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    dimensions = (9, 6)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((dimensions[0]*dimensions[1],3), np.float32)
    objp[:,:2] = np.mgrid[0:dimensions[0],0:dimensions[1]].T.reshape(-1,2)
    # Size of the chessboard squares in m
    objp *= 0.018

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    images = glob.glob('calib_*.jpg')

    print("Starting loop")
    for fname in images:
        img = cv2.imread(fname)
        img = cv2.flip(img, 0)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, dimensions, None)

        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, dimensions, corners2,ret)
            cv2.imshow('img',img)
            cv2.waitKey(500)
        else:
            cv2.imshow('img',gray)
            cv2.waitKey(500)
            print("Found no chessboard")

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

    print(tvecs)
    print("RVecs: ", rvecs)
    print(rvecs)
    np.save("camera_matrix", mtx)
    np.save("camera_distortion", dist)

    cv2.destroyAllWindows()


def run_tracker(profile=False):
    show_gui = False
    addr = None
    port = 4242
    if len(sys.argv) == 1:
        print("[warn] No remote address address specified, running GUI")
        show_gui = True
    elif len(sys.argv) == 2:
        addr = sys.argv[1]
        print("[info] Going to send to: {}".format(addr))
    elif len(sys.argv) == 3:
        addr = sys.argv[1]
        port = int(sys.argv[2])

    sock = None
    if addr is not None:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    debug_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    camera_matrix = np.load("camera_matrix.npy")
    camera_distortion = np.load("camera_distortion.npy")

    cam = cv2.VideoCapture(0)
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, RESOLUTION[0]);
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, RESOLUTION[1]);
    cam.set(cv2.CAP_PROP_FPS, 60);

    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()

    # Change thresholds
    params.minThreshold = 10
    params.maxThreshold = 200


    # Filter by Area.
    params.filterByArea = False
    # params.minArea = 1500

    # Filter by Circularity
    params.filterByCircularity = False
    # params.minCircularity = 0.1

    # Filter by Convexity
    params.filterByConvexity = False
    # params.minConvexity = 0.87

    # Filter by Inertia
    params.filterByInertia = False
    # params.minInertiaRatio = 0.01
    detector = cv2.SimpleBlobDetector_create(params);

    head_offset_x = -200;
    head_offset_y = 100;
    head_offset_z = 0;
    tracker_points = -np.array([
        [head_offset_x + 0.,  head_offset_y + -52.,   head_offset_z + -50.],
        [head_offset_x + 0.,  head_offset_y + 0.,     head_offset_z + 0.],
        [head_offset_x + 20., head_offset_y + 45.49, head_offset_z + -8.5],
        [head_offset_x + 0.,  head_offset_y + 88.,    head_offset_z + -25.],
    ], dtype='f') / 1000

    last_print = time.time();

    iterations = 0

    while True:
        start = time.time()
        iterations += 1
        if profile and iterations > 100:
            break
        (ret_val, img) = cam.read()
        img = cv2.flip(img, 0)
        # img = cv2.undistort(img, camera_matrix, camera_distortion)
        # cv2.imshow("raw", img)

        # hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        (_, thresholded) = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)
        # Filter the colour of the LEDs
        # lower_bound = (120, 0, 0);
        # upper_bound = (190, 255, 255);
        # filtered = cv2.inRange(hsv, lower_bound, upper_bound)
        # masked = cv2.bitwise_and(img, img, mask=filtered)
        # (_, thresholded) = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

        # Erode to get rid of unwanted noise
        erosion_size = 5;
        element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*erosion_size + 1, 2*erosion_size+1), (erosion_size, erosion_size))
        eroded = cv2.erode(thresholded, element)
        dilated = cv2.dilate(eroded, element)

        (contours, _) = cv2.findContours(dilated, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

        circles = list(map(lambda cnt: cv2.minEnclosingCircle(cnt), contours))

        # Blob detection works on False elements
        # keypoints = detector.detect(cv2.bitwise_not(thresholded))
        if show_gui:
            # with_keypoints = cv2.drawKeypoints(
            #     cv2.bitwise_not(dilated),
            #     keypoints,
            #     np.array([]),
            #     (0,0,255),
            #     cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
            # )
            print(len(contours))
            with_color = cv2.cvtColor(dilated, cv2.COLOR_GRAY2BGR)
            # with_contours = cv2.drawContours(with_color, contours, -1, (255, 0, 255), 2) 
            for ((x, y), radius) in circles:
                center = (int(x), int(y))
                radius = int(radius)
                with_color = cv2.circle(with_color, center, radius, (0, 0, 255), 1)
            # print("imshow")
            cv2.imshow("webcam", with_color)
            # cv2.imshow("raw", img)

        # Get the positions of the detected points
        # image_points = list(map(lambda p: p.pt, keypoints))
        image_points = list(map(lambda circle: circle[0], circles))
        # Sort the points in the y-direction
        image_points.sort(key = lambda p: -p[1])
        image_points = np.array([image_points])


        if len(image_points[0]) == 4:
            # (ret, rvec, tvec) = cv2.solvePnP(
            (ret, rvec, tvec) = cv2.solvePnP(
                tracker_points,
                image_points,
                camera_matrix,
                camera_distortion,
                # useExtrinsicGuess=True,
                # tvec=np.array([1,0,0], dtype='f'),
                # tvec=guess,
                # rvec=np.array([0,0,0], dtype='f'),
                # rvec = rguess,
                # flags = cv2.SOLVEPNP_ITERATIVE,
                flags = cv2.SOLVEPNP_AP3P,
                # reprojectionError = r
            );

            tvec *= 100;
            # rvec = rvec / math.pi * 360
            rvec_euler = rot_params_rv(rvec)
            # print(tvec)
            # print(rvec)
            # R = cv2.Rodrigues(rvec)[0]
            # R_inv = R.transpose()
            # tvec_camera = -R_inv @ tvec;
            # print(R_inv)
            # print(tvec_camera)
            # rotated_tvec = R @ tvec
            # message = struct.pack(
            #         "ffffffffffff",
            #         R[0, 0],
            #         R[0, 1],
            #         R[0, 2],
            #         R[1, 0],
            #         R[1, 1],
            #         R[1, 2],
            #         R[2, 0],
            #         R[2, 1],
            #         R[2, 2],
            #         tvec[0],
            #         tvec[1],
            #         tvec[2]
            #     )
            # debug_sock.sendto(message, ("192.168.0.106", 12345))

            # print("sending data")
            if time.time() - last_print > 0.5:
                # print(tvec)
                last_print = time.time();
                print("Tvec:", tvec)
                print("rvec:", rvec_euler)

            if sock is not None:
                message = struct.pack(
                        "dddddd",
                        tvec[0],
                        tvec[1],
                        tvec[2],
                        rvec_euler[1], # rvec_euler[1],
                        rvec_euler[0],
                        rvec_euler[2]# rvec_euler[2], # rvec_euler[2],
                    )
                print("Tvec:", tvec)
                print("rvec:", rvec_euler)
                print(len(message))
                for byte in message:
                    print(byte, end = ", ")
                print("")
                sock.sendto(message, (addr, port))

        else:
            if time.time() - last_print > 0.5:
                print("Unexpected amount of image points: ", len(image_points))
                last_print = time.time();


        if show_gui:
            if cv2.waitKey(1) == 27:
                break

        end = time.time()
        print("Iteration time: ", end-start)




if __name__ == "__main__":
    # capture_calibration()
    # calibrate()
    run_tracker()
    # cProfile.run('run_tracker()')
